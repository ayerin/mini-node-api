# Mini Node API

This is a multi-purposes sample Node.js based project intended to be used for experimentation.

### How to use this project

This project contains a healthcheck route with 2 endpoints `healthz` and `info`. For extended use cases in different environments, ones can fork this project and implement yours

> The configuration is hardcoded in this project. Feel free to adjust to fit your needs
