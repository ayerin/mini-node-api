const express = require('express');
const logger = require('morgan');

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(logger('dev'));

app.use('/', require('./routes/common'));

app.listen(3000, () => console.log('The application is running on the port %s', 3000));