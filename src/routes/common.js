const dayjs = require('dayjs');
const { Router } = require('express');

const router = Router();

router.get('/healthz', (_, res) => {
  return res.json({
    status: '000',
    message: 'Success',
  })
});

router.get('/info', (_, res) => {
  return res.json({
    status: '000',
    message: 'Success',
    data: {
      time: dayjs().toISOString(),
    }
  });
});

module.exports = router;